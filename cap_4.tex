\selectlanguage{italian}
\chapter{TestBed}
\thispagestyle{empty}
\newpage
\section{Premessa}
Quest'ultimo capitolo vuole essere a scopo introduttivo e vuole avvicinare il lettore all'immediato utilizzo del framework Hyperledger Fabric. Dopo averne descritto le caratteristiche essenziali, rimandando alla documentazione ufficiale per ulteriori approfondimenti, si focalizzerà l'attenzione sui progetti collaterali nati per la realizzazione di un TestBed efficace ed esplicativo in termini di simulazione di uno scenario ove i tre attori precedenti (IoT, IdM e Fabric) interagiscono ed in termini di prestazioni rispetto ad uno scenario con dispositivi IoT fisici. 
In ordine verranno mostrati:
\begin{itemize}
	\item \textbf{SimOnPi}
	\item \textbf{Kernel ottimizzati per QEMU}
	\item \textbf{Chaincode Hyperledger Fabric}
\end{itemize}
È da sottolineare e ribadire, ancora una volta, che verrà preso in considerazione un modello di identità molto semplice dove un \textit{thing} viene identificato da molteplici descrittori. Tale semplificazione servirà a capire i meccanismi di Fabric in maniera più semplice.
\section{SimOnPi}
SimOnPi, a dispetto del nome, non è un simulatore. Si tratta di uno script di frontend per il noto progetto denominato QEMU, atto all'emulazione di tutte le famiglie di dispositivi Raspberry Pi.
SimOnPi è nato dalla necessità di creare uno strumento di generazione ed esecuzione di immagini compatibili con tutte le famiglie di Raspberry Pi, astraendo e velocizzando il processo di argomenti validi per l'emulazione da passare a QEMU.
Raspberry Pi è un single-board computer realizzato in UK e largamente usato in ambiente IoT. Le motivazioni che hanno fatto ricadere la scelta su tale dispositivo IoT per la fase di TestBed sono le seguenti:
\begin{itemize}
	\item \textbf{Popolarità} \texttt{-} A cinque anni dal suo lancio, nel 2012, Raspberry Pi detiene il record di terzo computer più venduto di tutti i tempi, a livello di macrocategoria, dopo PC e MAC, superando il volume di vendite del celebre Commodore 64.
	\item \textbf{Basso costo} \texttt{-} Nel corso degli anni la Pi Foundation ha sempre avuto un occhio di riguardo per il prezzo, facendo evolvere le specifiche senza gonfiare i prezzi tra un modello e l'altro.
	\item \textbf{Supporto vasto} \texttt{-} Essendo un progetto che mette a disposizione approfonditi dettagli tecnici sull'hardware, molto usato nell'ambito educativo e attorniato da una pletora di contributi dal mondo open source, la vasta comunità di utilizzatori offre un supporto molto dettagliato, una grande libreria di progetti utilizzabili da chiunque, senza sovrapprezzi e con una curva di apprendimento molto rapida. Raspberry Pi è in grado, inoltre, di eseguire le più popolari e conosciute distribuzioni GNU/Linux.
	\item \textbf{Risorse non troppo limitate} \texttt{-} Raspberry Pi ha caratteristiche tipiche di un single-board computer, ovvero presenta risorse di calcolo limitate, tuttavia la scelta è ricaduta su di esso per semplificare l'uso di Fabric in un ambiente che non sia troppo limitato e limitante (come un sensore di temperatura).
\end{itemize}
QEMU (abbreviazione di Quick EMUlator) è un software che implementa un particolare sistema di emulazione, che permette di ottenere un'architettura informatica nuova e disgiunta (guest) in un'altra, che si occuperà di ospitarla (host). Questo software è conosciuto per merito della sua velocità di esecuzione, ottenuta grazie alla tecnica della traduzione dinamica (o ricompilazione dinamica). Tale espediente consiste nell'ottimizzazione dei programmi emulati tramite analisi e ricompilazione in fase di esecuzione.
SimOnPi usa l'ultima versione stabile di QEMU (ad ora 2.12) per offrire le seguenti funzionalità:
\begin{itemize}
	\item Creazione dell'immagine SDcard per Raspberry Pi.
	\item Esecuzione dell'immagine su architettura desiderata.
	\item Integrazione di pacchetti esterni nell'immagine in fase di creazione.
	\item Integrazione di file generici esterni nell'immagine in fase di creazione.
	\item Funzionalità di rete per le macchine guest emulate.
	\item Supporto ad UEFI Secure Boot tramite firmware OVMF \cite{ovmf}.
	\item Accesso SSH alle macchine guest emulate.
	\item Accelerazione KVM e VirtIO complete laddove possibile.
	\item Gestione delle immagini e delle risorse usate in fase di creazione dell'immagine.
	\item Esecuzione in container Docker.
	\item Deployment diretto dell'immagine su SDcard fisica e su dispositivo fisico.
\end{itemize}
Poichè SimOnPi supporta la tabella delle partizioni predefinita usata da ogni revisione di Raspberry Pi \cite{parts}, è possibile eseguire qualsiasi distribuzione GNU/Linux correttamente installata. Per il seguente elaborato, la scelta è ricaduta su Arch Linux Arm come distribuzione prediletta da SimOnPi. Seguono una panoramica delle opzioni disponibili da linea di comando e delle prime istruzioni per iniziare.
\begin{figure}[ht]
	\centering					
	\includegraphics[scale=1.8]{cli}
	\caption{Comandi disponibili in SimOnPi.}
	\label{fig:cli}
\end{figure}
A meno che non si abbia a disposizione un'immagine già generata (come quella disponibile per Raspbian), come si evince dalla Figura \ref{fig:cli}, il primo passo da eseguire è il comando\newline \textbf{simonpi MODEL -s SIZE}\newline
con MODEL che comprende i seguenti valori:
\begin{itemize}
	\item \textbf{rpi} \texttt{-} per la prima famiglia con architettura ARMv6.
	\item \textbf{rpi-2} \texttt{-} per la seconda famiglia con architettura ARMv7.
	\item \textbf{rpi-3} \texttt{-} per la terza famiglia con architettura ARMv8 a 32 e 64 bit.
\end{itemize}
Con l'argomento \textbf{-s} si specifica la dimensione in GB dell'immagine SDcard da creare.
Prima di lanciare il programma bisogna accertarsi di essere connessi ad Internet.
SimOnPi provvederà a fare le seguenti operazioni, descrivendole sullo standard output del terminale:
\begin{enumerate}
	\item Controllo degli eseguibili richiesti.
	\item Controllo di immagini già esistenti.
	\item Download dell'archivio Arch Linux ARM per l'architettura specificata con MODEL.
	\item Verifica dell'integrità dell'archivio con somma di controllo MD5.
	\item Creazione dell'immagine vuota e senza tabella delle partizioni.
	\item Montaggio dell'immagine e scrittura della tabella delle partizioni, in particolare:
	      \begin{itemize}
	      	\item partizione di boot, con filesystem W95 FAT32 (LBA), primaria e di circa 100MB
	      	\item partizione di sistema, con filesystem EXT4, primaria ed estesa al massimo dei GB specificati con \textbf{SIZE}
	      \end{itemize}
	\item Montaggio delle partizioni.
	\item Estrazione dell'archivio Arch Linux ARM nelle partizioni predisposte.
	\item Integrazioni dei pacchetti, se presenti e compatibili col package manager di Arch Linux (pacman), posizionati nella cartella $\sim$/.simonpi/\textbf{MODEL}/pkgs.
	\item Sincronizzazione dei buffer di scrittura e smontaggio delle partizioni.
\end{enumerate}
Le immagini, così create, vengono memorizzate e organizzate in sottocartelle nel percorso della home dell'utente corrente e sotto la cartella nascosta \textbf{.simonpi}. Nell'esecuzione dello script l'uso di privilegi di root è richiesto solo in alcune fasi tramite l'esecuzione del comando \textbf{sudo} e minimizzato per quanto possibile, come da buona prassi su sistemi UNIX.
Successivamente l'utente potrà eseguire l'emulazione dell'immagine appena creata digitando il comando:
\textbf{simonpi MODEL -r}\newline
oppure\newline
\textbf{simonpi MODEL -i PATH}\newline
dove \textbf{PATH} rappresenta il percorso di una immagine già data (per esempio \textbf{simonpi rpi-3 -i /home/utente/2018-06-27-raspbian-stretch.img})
Verranno eseguite le seguenti operazioni:
\begin{enumerate}
	\item Rilevamento delle partizioni sull'immagine data.
	\item Montaggio dell'immagine.
	\item Controllo di integrità dei filesystem sulle 2 partizioni, per evitare fallimenti o corruzioni indesiderate.
	\item Montaggio delle partizioni.
	\item Valutazione delle interfacce di rete e del loro stato, con rilevamento dell'esecuzione in container Docker. Ogni guest istanzierà un'interfaccia di rete di tipo TUN/TAP (\textbf{rasp-tapX}), automaticamente associata ad un'interfaccia principale di tipo \textit{bridge} (\textbf{rasp-br0}). In tal modo più guest possono comunicare tra di loro in localhost. Con il supporto a Docker è molto facile costruire e comporre topologie di guest ARM tramite l'uso di Docker Compose.
	\item Abilitazione della rete, con attivazione del server dnsmasq per la risoluzione degli indirizzi all'interno delle macchine guest.
	\item Caricamento dell'immagine OVMF per supporto ad UEFI.
	\item Caricamento del kernel Linux come eseguibile EFI.
\end{enumerate}
È da specificare che SimOnPi è in grado di usare le funzionalità di accelerazione hardware per l'emulazione (si vedano gli istruction-set per la virtualizzazione e VT-x) in maniera completa soprattutto per l'ultima famiglia basata su architettura ARMv8.
Gli altri comandi sono riportati sinteticamente di seguito:
\begin{itemize}
	\item \textbf{-c} \texttt{-} Controlla l'integrità dei filesystem delle 2 partizioni d'immagine.
	\item \textbf{-e} \texttt{-} Cancella immagini SD e archivi scaricati per il MODEL specificato.
	\item \textbf{-k} \texttt{-} Uccide ogni istanza indesiderata di SimOnPi per il MODEL specificato, ripristina i parametri di rete e prova a smontare le partizioni.
	\item \textbf{-l} \texttt{-} Mostra una lista dei file archiviati per il MODEL specificato.
	\item \textbf{-m} \texttt{-} Monta le partizioni per il MODEL specificato.
	\item \textbf{-p} \texttt{-} Cancella solo le immagini SD per il MODEL specificato, senza toccare gli archivi scaricati.
	\item \textbf{-u} \texttt{-} Smonta le partizioni per il MODEL specificato.
	\item \textbf{-v} \texttt{-} Mostra le note di versione e l'autore.
\end{itemize}
Una volta generata l'immagine SD con il proprio contenuto e le dovute personalizzazioni, è possibile scrivere l'immagine su di una SD card fisica tramite l'uso del comando \textbf{dd} o del più user-friendly \textbf{Etcher}. Si riporta qui un esempio di linea di comando per il trasferimento suddetto:
\begin{center}
	dd if=/home/utente/.simonpi/sd-arch-rpi-3-qemu.img of=/dev/mmcblk0 bs=8M
\end{center}
\begin{itemize}
	\item	\textbf{dd} \texttt{-} Comando dei sistemi operativi Unix e Unix-like, e più in generale dei sistemi POSIX, che copia dei dati in blocchi, opzionalmente effettuando conversioni. 
	\item \textbf{if} \texttt{-} Legge i dati dal file \textit{sd-arch-rpi-3-qemu.img} invece che dallo standard input.
	\item \textbf{of} \texttt{-} Scrive i dati sul file \textit{mmcblk0}, nome di una ipotetica SDcard, invece che sullo standard output.
	\item \textbf{bs} \texttt{-} Legge e scrive dati in blocchi delle dimensioni specificate, in questo caso \textit{8M} (opzionale, richiesto per aumentare la velocità di trasferimento).
\end{itemize} 
Per rendere possibile tutto ciò SimOnPi ha dato i natali ad un altro progetto collaterale costituito da tre kernel Linux ottimizzati per l'emulazione su QEMU.

\subsection{Kernel ottimizzati per QEMU}
Piccola, ma doverosa sezione, meritano i kernel Linux ottimizzati per l'emulazione di Raspberry Pi tramite QEMU. Tale sviluppo è nato dalla necessità di avere, in primo luogo, una buona emulazione per la prima famiglia di Raspberry Pi con un kernel recente. Il lavoro ha preso larga ispirazione da una patch rilasciata da XecDesign \cite{xec} per la corretta compilazione del kernel per QEMU, non più disponibile e ormai non adatta alle recenti versioni del kernel Linux. Tale patch consente di forzare l'architettura arm da usare per la corretta emulazione di QEMU. Da qui il lavoro si è esteso alle altre 2 famiglie con buoni risultati e senza dover modificare i sorgenti originali. Qui \cite{qemukernels} sono disponibili gli script di generazione delle immagini binarie, le immagini binarie pronte all'uso, le configurazioni predefinite e la patch per la prima famiglia. Ad oggi i kernel sono sincronizzati alla versione 4.17. Quando SimOnPi carica l'immagine OVMF, essa cerca automaticamente per l'immagine del Kernel valida presente nel percorso stabilito di default dallo script e lo lancia come binario EFI compatibile, esterno all'immagine SD del guest. In Figura \ref{fig:flow} è mostrato il flusso di esecuzione di SimOnPi e dei Kernel.
\begin{figure}[ht]
	\centering					
	\includegraphics{flow}
	\caption{Flusso di esecuzione di SimOnPi.}
	\label{fig:flow}
\end{figure}
\subsubsection{Peer Fabric su ARM}
La fase successiva si è concentrata sull'isolamento della parte \textit{peer} di Hyperledger Fabric, con conseguenziale produzione di una semplice modifica del file di compilazione per il supporto alle architetture ARM. L'intero codice di Hyperledger Fabric è stato sviluppato in Go \cite{go}. Ispezionando il Makefile della parte \textit{peer} si è giunti all'impostazione della variabile \textbf{GOARCH} ai valori \textbf{arm} e \textbf{arm64} per la cross-compilazione degli eseguibili richiesti. La patch per il supporto ad ARM è disponibile nel materiale correlato al seguente elaborato. 
SimOnPi ha permesso, dunque, la pacchettizzazione della parte \textit{peer} di Hyperledger Fabric per Arch Linux Arm \cite{aur}, nonchè il testing del chaincode da nodo IoT.
\section{Esecuzione dello scenario}
È stato realizzato uno scenario (Figura \ref{fig:topo}) composto dai seguenti attori:
\begin{itemize}
	\item Un'organizzazione Orderer.
	\item Un' organizzazione (chiamata ACME) che raggruppa 2 peer istanziati immediatamente.
	\item Un client/peer in IoT che deve identificarsi e autenticarsi in un secondo momento, con uno schema semplificato simile a quello descritto in Figura \ref{fig:thingids}.
\end{itemize}
\begin{figure}[ht]
	\centering					
	\includegraphics{topo}
	\caption{Scenario considerato.}
	\label{fig:topo}
\end{figure}
La topologia della rete e il canale di comunicazione istaurato tra i peer vengono determinati rispettivamente in 2 file di estensione \textit{.yaml}:
\begin{itemize}
	\item \textbf{crypto-config.yaml} \texttt{-} Tale file serve per la generazione del materiale crittografico di ogni parte, dunque per il bootstrap della rete blockchain. Esso viene elaborato dal comando cryptogen, il quale ripone il materiale in strutture organizzate sotto la cartella \textbf{crypto-config}.   
	\item \textbf{configtx.yaml} \texttt{-} Tale file serve, dopo aver generato il materiale crittografico, per generare un blocco di genesi dell'orderer. Un blocco di genesi è il blocco di configurazione che inizializza una rete o un canale blockchain e funge anche da primo blocco su una catena. Questo può essere fatto usando lo strumento \textbf{configtxgen} che accetta configtx.yaml come input.
\end{itemize}
La rete viene instanziata eseguendo \textbf{docker-compose} tramite i file descrittori .\textit{yaml} di cui è corredato questo elaborato di tesi. In particolare, per semplificare al lettore tali operazioni, si potrà scegliere tra:
\begin{itemize}
	\item \textbf{start-tls} \texttt{-} Scenario provvisto di comunicazione end-to-end e crittografia TLS tra i vari attori. peer2 è un container tradizionale.
	\item \textbf{start-tls-arm} \texttt{-} Scenario uguale al precendente, ma con peer2 su container con emulazione ARM.
\end{itemize}
Prendendo in considerazione il secondo, all'inizio del processo viene generato il \textit{genesis block}, il materiale crittografico per MSP e peer. Successivamente, a orderer inizializzato, seguono peer0 e peer1, che si uniscono al canale denominato \textit{mychannel}.
peer2 si unisce al canale nel modo seguente:
\begin{enumerate}
	\item Lancio del container \textbf{simonpi}. Tale container, di predefinito, è un'istanza di SimOnPi per Raspberry Pi 3.
	\item Passaggio del materiale crittografico di peer2, precedentemente generato, tramite il meccanismo di \textit{volumes} disponibile su Docker.
	\item peer2 richiede di essere aggiunto al canale \textit{mychannel}.
	\item Esecuzione di un programma senza interazione denominato \textbf{iotdevices}: esso di occupa di generare degli identificativi univoci da associare all'identità peer2.
	      \textbf{iotdevices} è scritto in Go e permette di forgiare gli argomenti adatti all'immagazzinamento delle informazioni nel ledger della blockchain.
	\item Esecuzione di \textbf{peer chaincode instantiate} per il salvataggio delle informazioni su blockchain.
\end{enumerate}.
Per ogni peer appartenente a \textit{mychannel}, viene instanziato un container contenente il database di stato. Poichè, come detto in precedenza, sono innestabili diversi moduli per la parte database, la scelta di questo elaborato è ricaduta su \textbf{CouchDB} anzichè su LevelDB, scelta predefinita per Fabric, per alcuni vantaggi che vi risiedono. In particolare:
\begin{itemize}
	\item Fornisce \textit{eventual consistency} tra i database.
	\item Possibilità di eseguire query molto complesse. È buona pratica modellare gli asset del chaincode in JSON, per poter sfruttare tale funzionalità.
\end{itemize} 
A questo punto sarà possibile visualizzare i risultati della query eseguita dal peer2, tramite l'interfaccia web messa a disposizione dal container CouchDB e denominata Fauxton.
Fauxton apre il servizio su porta \textbf{5984}.


\section{Prestazioni}
\section{Risultati}
 
