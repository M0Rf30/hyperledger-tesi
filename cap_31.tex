\selectlanguage{italian}
\chapter{TestBed}
\thispagestyle{empty}
\newpage
\section{Premessa}
In quest'ultimo capitolo verrà descritto l'applicativo realizzato per la simulazione di una topologia di rete distribuita IoT (denominato SimOnPi), integrata con il framework LwM2M di Eclipse Foundation (denominato Leshan) e verranno dati tutti i riferimenti per avvicinare il lettore all'immediata replicazione del TestBed. Dopo averne descritto le caratteristiche essenziali, rimandando alla documentazione ufficiale per ulteriori approfondimenti, si focalizzerà l'attenzione sui progetti collaterali, nati per la realizzazione di un TestBed efficace in termini di simulazione, e su di uno scenario, ove i due attori precedenti (IoT e LwM2M) interagiscono.
\section{SimOnPi}
SimOnPi, a dispetto del nome, non è un simulatore. Si tratta di uno script \newacronym{bash}{BASH}{Bourne Again SHell}\acrshort{bash} di frontend per il noto progetto denominato \newacronym{qemu}{QEMU}{Quick Emulator}\acrshort{qemu} (Quick Emulator), atto all'emulazione di tutte le famiglie di dispositivi Raspberry Pi.
SimOnPi è nato dalla necessità di creare uno strumento di generazione ed esecuzione di immagini compatibili con tutte le famiglie di Raspberry Pi, astraendo e velocizzando il processo di argomenti validi per l'emulazione da passare a QEMU.
Raspberry Pi è un single-board computer realizzato in UK e largamente usato in ambiente IoT. Le motivazioni che hanno fatto ricadere la scelta su tale dispositivo IoT per la fase di TestBed sono le seguenti:
\begin{itemize}
	\item \textbf{Popolarità} - A cinque anni dal suo lancio, nel 2012, Raspberry Pi detiene il record di terzo computer più venduto di tutti i tempi, a livello di macrocategoria, dopo PC e MAC, superando il volume di vendite del celebre Commodore 64.
	\item \textbf{Basso costo} - Nel corso degli anni la Pi Foundation ha sempre avuto un occhio di riguardo per il prezzo, facendo evolvere le specifiche senza gonfiare i prezzi tra un modello e l'altro.
	\item \textbf{Supporto vasto} - Essendo un progetto che mette a disposizione approfonditi dettagli tecnici sull'hardware, molto usato nell'ambito educativo e attorniato da una pletora di contributi dal mondo open source, la vasta comunità di utilizzatori offre un supporto molto dettagliato, una grande libreria di progetti utilizzabili da chiunque, senza sovrapprezzi e con una curva di apprendimento molto rapida. Raspberry Pi è in grado, inoltre, di eseguire le più popolari e conosciute distribuzioni GNU/Linux.
	\item \textbf{Risorse non troppo limitate} - Raspberry Pi ha caratteristiche tipiche di un single-board computer, ovvero presenta risorse di calcolo limitate, tuttavia la scelta è ricaduta su di esso per semplificare l'uso di Fabric in un ambiente che non sia troppo limitato e limitante (come un sensore di temperatura).
\end{itemize}
Il software su cui si basa SimOnPi, denominato \newacronym{qemu}{QEMU}{Quick Emulator}\acrshort{qemu}, è in grado di implementare un particolare sistema di emulazione, che permette di ottenere un'architettura informatica nuova e disgiunta (guest) in un'altra, che si occuperà di ospitarla (host). Questo software è conosciuto per merito della sua velocità di esecuzione, ottenuta grazie alla tecnica della traduzione dinamica (o ricompilazione dinamica). Tale espediente consiste nell'ottimizzazione dei programmi emulati tramite analisi e ricompilazione in fase di esecuzione.
SimOnPi usa l'ultima versione stabile di QEMU (ad ora 3.10) per offrire le seguenti funzionalità:
\begin{itemize}
	\item Creazione dell'immagine SDcard per Raspberry Pi.
	\item Esecuzione dell'immagine su architettura desiderata.
	\item Integrazione di pacchetti esterni nell'immagine in fase di creazione.
	\item Integrazione di file generici esterni nell'immagine in fase di creazione.
	\item Funzionalità di rete per le macchine guest emulate.
	\item Supporto ad \newacronym{uefi}{UEFI}{Unified Extensible Firmware Interface}\acrshort{uefi} Secure Boot tramite firmware \newacronym{ovmf}{OVMF}{Open Virtual Machine Firmware}\acrshort{ovmf} (Open Virtual Machine Firmware) \cite{ovmf}.
	\item Accesso \newacronym{ssh}{SSH}{Secure SHell}\acrshort{ssh} alle macchine guest emulate.
	\item Accelerazione \newacronym{kvm}{KVM}{Kernel-based Virtual Machine}\acrshort{kvm} (Kernel-based Virtual Machine) e VirtIO \cite{virtio} complete laddove possibile.
	\item Gestione delle immagini e delle risorse usate in fase di creazione dell'immagine.
	\item Esecuzione in container Docker \cite{docker}.
	\item Deployment diretto dell'immagine su SDcard fisica e su dispositivo fisico.
\end{itemize}
Poichè SimOnPi supporta la tabella delle partizioni predefinita usata da ogni revisione di Raspberry Pi \cite{parts}, è possibile eseguire qualsiasi distribuzione GNU/Linux correttamente installata. Per il seguente elaborato, la scelta è ricaduta su Arch Linux Arm come distribuzione prediletta da SimOnPi. Seguono una panoramica delle opzioni disponibili da linea di comando e delle prime istruzioni per iniziare.
\begin{figure}[ht]
	\centering					
	\includegraphics[scale=1.8]{cli}
	\caption{Comandi disponibili in SimOnPi}
	\label{fig:cli}
\end{figure}
A meno che non si abbia a disposizione un'immagine già generata (come quella disponibile per Raspbian), come si evince dalla Figura \ref{fig:cli}, il primo passo da eseguire è il comando\newline \texttt{simonpi MODEL -s SIZE}\newline
con MODEL che comprende i seguenti valori:
\begin{itemize}
	\item \textbf{rpi} - per la prima famiglia con architettura ARMv6.
	\item \textbf{rpi-2} - per la seconda famiglia con architettura ARMv7.
	\item \textbf{rpi-3} - per la terza famiglia con architettura ARMv8 a 32 e 64 bit.
\end{itemize}
Con l'argomento \texttt{-s} si specifica la dimensione in GB dell'immagine SDcard da creare.
Prima di lanciare il programma bisogna accertarsi di essere connessi ad Internet.
SimOnPi provvederà a fare le seguenti operazioni, descrivendole sullo standard output del terminale:
\begin{enumerate}
	\item Controllo degli eseguibili richiesti.
	\item Controllo di immagini già esistenti.
	\item Download dell'archivio Arch Linux ARM per l'architettura specificata con MODEL.
	\item Verifica dell'integrità dell'archivio con somma di controllo MD5.
	\item Creazione dell'immagine vuota e senza tabella delle partizioni.
	\item Montaggio dell'immagine e scrittura della tabella delle partizioni, in particolare:
	      \begin{itemize}
	      	\item partizione di boot, con filesystem W95 FAT32 (LBA), primaria e di circa 100MB
	      	\item partizione di sistema, con filesystem EXT4, primaria ed estesa al massimo dei GB specificati con \texttt{SIZE}
	      \end{itemize}
	\item Montaggio delle partizioni.
	\item Estrazione dell'archivio Arch Linux ARM nelle partizioni predisposte.
	\item Integrazioni dei pacchetti, se presenti e compatibili col package manager di Arch Linux (pacman), posizionati nella cartella \texttt{/.simonpi/MODEL/pkgs}.
	\item Sincronizzazione dei buffer di scrittura e smontaggio delle partizioni.
\end{enumerate}
Le immagini, così create, vengono memorizzate e organizzate in sottocartelle nel percorso della home dell'utente corrente e sotto la cartella nascosta \texttt{.simonpi}. Nell'esecuzione dello script l'uso di privilegi di root è richiesto solo in alcune fasi tramite l'esecuzione del comando \texttt{sudo} e minimizzato per quanto possibile, come da buona prassi su sistemi UNIX.
Successivamente l'utente potrà eseguire l'emulazione dell'immagine appena creata digitando il comando:
\begin{center}
\texttt{simonpi MODEL -r}\newline
oppure\newline
\texttt{simonpi MODEL -i PATH}\newline
\end{center}
dove \texttt{PATH} rappresenta il percorso di una immagine già data. Come esempio da linea di comando:
\begin{center}
\texttt{simonpi rpi-3 -i /home/utente/2018-06-27-raspbian-stretch.img}
\end{center}
Verranno eseguite le seguenti operazioni:
\begin{enumerate}
	\item Rilevamento delle partizioni sull'immagine data.
	\item Montaggio dell'immagine.
	\item Controllo di integrità dei filesystem sulle 2 partizioni, per evitare fallimenti o corruzioni indesiderate.
	\item Montaggio delle partizioni.
	\item Valutazione delle interfacce di rete e del loro stato, con rilevamento dell'esecuzione in container Docker. Ogni guest istanzierà un'interfaccia di rete di tipo TUN/TAP (\textbf{rasp-tapX}), automaticamente associata ad un'interfaccia principale di tipo bridge (\textbf{rasp-br0}). In tal modo più guest possono comunicare tra di loro in locale o in ambienti cloud predisposti per l'esecuzione di container. Con il supporto a Docker è molto facile costruire e comporre topologie di macchine virtuali con architettura ARM, tramite l'uso di Docker Compose \cite{dockercompose}.
	\item Abilitazione della rete, con attivazione del server dnsmasq, con funzionalità di \newacronym{dhcp}{DHCP}{Dynamic Host Configuration Protocol}\acrshort{dhcp} e cache \newacronym{dns}{DNS}{Domain Name System}\acrshort{dns}, per la risoluzione degli indirizzi all'interno delle macchine guest.
	\item Caricamento dell'immagine \newacronym{ovmf}{OVMF}{Open Virtual Machine Firmware}\acrshort{ovmf} per supporto ad UEFI.
	\item Caricamento del kernel Linux come eseguibile EFI.
\end{enumerate}
È da specificare che SimOnPi è in grado di usare le funzionalità di accelerazione hardware per l'emulazione (si vedano gli istruction-set per la virtualizzazione e VT-x) in maniera completa soprattutto per l'ultima famiglia basata su architettura ARMv8. Doverosa precisazione per il lettore è che Docker, di per sè, non attua un tipo di virtualizzazione pesante, essendo sprovvisto del supervisor tipico delle tradizionali tecnologie \newacronym{vmm}{VMM}{Virtual Machine Manager}\acrshort{vmm} (Virtual Machine Manager). Docker è un progetto open-source che automatizza il deployment di applicazioni all'interno di contenitori software, fornendo un'astrazione aggiuntiva grazie alla virtualizzazione a livello di kernel Linux, e Docker Compose è uno strumento che permette di eseguire uno scenario di più container, anche interconnessi. Tuttavia, le istanze di SimOnPi in container includono QEMU e, dunque, è giusto parlare di emulazione architetturale all'interno dei container.
Gli altri comandi sono riportati sinteticamente di seguito:
\begin{itemize}
	\item \textbf{-c} - Controlla l'integrità dei filesystem delle 2 partizioni d'immagine.
	\item \textbf{-e} - Cancella immagini SD e archivi scaricati per il MODEL specificato.
	\item \textbf{-k} - Uccide ogni istanza indesiderata di SimOnPi per il MODEL specificato, ripristina i parametri di rete e prova a smontare le partizioni.
	\item \textbf{-l} - Mostra una lista dei file archiviati per il MODEL specificato.
	\item \textbf{-m} - Monta le partizioni per il MODEL specificato.
	\item \textbf{-p} - Cancella solo le immagini SD per il MODEL specificato, senza toccare gli archivi scaricati.
	\item \textbf{-u} - Smonta le partizioni per il MODEL specificato.
	\item \textbf{-v} - Mostra le note di versione e l'autore.
\end{itemize}
Una volta generata l'immagine SD con il proprio contenuto e le dovute personalizzazioni, è possibile scrivere l'immagine su di una SD card fisica tramite l'uso del comando \texttt{dd} o del più user-friendly Etcher. Si riporta qui un esempio di linea di comando per il trasferimento suddetto:
\begin{center}
	\texttt{dd if=/home/utente/.simonpi/sd-arch-rpi-3-qemu.img of=/dev/mmcblk0 bs=8M}
\end{center}
\begin{itemize}
	\item	\textbf{dd} - Comando dei sistemi operativi Unix e Unix-like, e più in generale dei sistemi POSIX, che copia dei dati in blocchi, opzionalmente effettuando conversioni. 
	\item \textbf{if} - Legge i dati dal file \texttt{sd-arch-rpi-3-qemu.img} invece che dallo standard input.
	\item \textbf{of} - Scrive i dati sul file \texttt{mmcblk0}, nome di una ipotetica SDcard, invece che sullo standard output.
	\item \textbf{bs} - Legge e scrive dati in blocchi delle dimensioni specificate, in questo caso \texttt{8M} (opzionale, richiesto per aumentare la velocità di trasferimento).
\end{itemize} 
Per rendere possibile tutto ciò, dalla realizzazione di SimOnPi è derivato ad un altro progetto costituito da tre kernel Linux ottimizzati per l'emulazione su QEMU.

\subsection{Kernel ottimizzati per QEMU}
Piccola, ma doverosa sezione, meritano i kernel Linux ottimizzati per l'emulazione di Raspberry Pi tramite \newacronym{qemu}{QEMU}{Quick Emulator}\acrshort{qemu}. Tale sviluppo è nato dalla necessità di avere, in primo luogo, una buona emulazione per la prima famiglia di Raspberry Pi con un kernel recente. Il lavoro ha preso larga ispirazione da una patch rilasciata da XecDesign \cite{xec} per la corretta compilazione del kernel per QEMU, non più disponibile e ormai non adatta alle recenti versioni del kernel Linux. Tale patch consente di forzare l'architettura arm da usare per la corretta emulazione di QEMU. Da qui il lavoro si è esteso alle altre 2 famiglie con buoni risultati e senza dover modificare i sorgenti originali. Qui \cite{qemukernels} sono disponibili gli script di generazione delle immagini binarie, le immagini binarie pronte all'uso, le configurazioni predefinite e la patch per la prima famiglia di Raspberry. Ad oggi i kernel sono sincronizzati alla versione 5.0 di Linux. Quando SimOnPi carica l'immagine \newacronym{ovmf}{OVMF}{Open Virtual Machine Firmware}\acrshort{ovmf}, essa cerca automaticamente per l'immagine del kernel valida presente nel percorso stabilito di default dallo script e lo lancia come binario EFI compatibile, esterno all'immagine SD del guest. In Figura \ref{fig:flow} è mostrato il flusso di esecuzione di SimOnPi e dei kernel.
\begin{figure}[ht]
	\centering					
	\includegraphics[scale=1.8,keepaspectratio]{flow}
	\caption{Flusso di esecuzione di SimOnPi}
	\label{fig:flow}
\end{figure}
\section{Esecuzione dello scenario}
Dopo aver descritto l'applicativo SimOnPi, il passo successivo consiste nel definire le componenti dello scenario realizzato ed il tipo di tecnologia utilizzata per la simulazione di una topologia di rete dove più client \newacronym{lwm2m}{LwM2M}{Lightweight Machine2Machine}\acrshort{lwm2m} (Lightweight Machine2Machine) interagiscono con la parte server.
La scelta è ricaduta su Leshan come piattaforma di integrazione per il lavoro di tesi principalmente per la sua prominenza e maturità tra le implementazioni open source disponibili. È considerata un'implementazione di riferimento per \newacronym{lwm2m}{LwM2M}{Lightweight Machine2Machine}\acrshort{lwm2m} \cite{putera}, ha una buona copertura della specifica \newacronym{lwm2m}{LwM2M}{Lightweight Machine2Machine}\acrshort{lwm2m} e una comunità di sviluppo attiva.  Essendo basato su Java, la sua piattaforma ei requisiti di runtime sono facilmente utilizzabili e consolidati su qualsiasi architettura, compresa ARM. Ciò ha permesso l'integrazione sui nodi Docker, istanziati da SimOnPi, senza dover ricorrere ad alcuna cross-compilazione.
In Figura \ref{fig:simonpiarch} è riportata la topologia di rete esaminata nel seguente lavoro:
\begin{figure}[ht]
	\centering					
	\includegraphics[width=10cm,keepaspectratio]{simonpiarch}
	\caption{Topologia di rete LwM2M, realizzata con SimOnPi}
	\label{fig:simonpiarch}
\end{figure}
La topologia istanziata, mediante l'uso del supporto di Docker di cui è provvisto SimOnPi, è costituita dai seguenti elementi
\begin{itemize}
	\item	\textbf{Server} - In tale container è istanziata una macchina virtuale con architettura emulata aarch64, ovvero Raspberry Pi 3, e la parte server di Leshan.
	\item \textbf{Nodo Client 1} - In tale container è istanziata una macchina virtuale con architettura emulata aarch64, ovvero Raspberry Pi 3, e la parte client di Leshan, modificata apppositamente per rappresentare un oggetto con ID 3303, ovvero un sensore di temperatura, secondo il registro IPSO.
	\item \textbf{Nodo Client 2} - In tale container è istanziata una macchina virtuale con architettura emulata armv6, ovvero Raspberry Pi 2, e la parte client di Leshan, modificata apppositamente per rappresentare un oggetto con ID 3303, ovvero un sensore di temperatura, secondo il registro IPSO.
\end{itemize}
\section{Risultati}
SimOnPi ha permesso, in maniera semplice ed immediata, l'utilizzo delle tecnologie di rete distribuita LwM2M su un insieme di nodi tipici dell'IoT, pur senza essere in possesso di hardware fisico o sensori di temperatura reali. Nonostante l'attenzione posta per ridurre l'eventuale overhead dovuto a virtualizzazione e uso dei container, si rimandano al lettore e agli eventuali sviluppi futuri le misurazioni delle prestazioni, rispetto ad uno scenario provvisto di hardware reale.
 
